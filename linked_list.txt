﻿EXPERIMENT-7


AIM:  Menu driven program for insertion in beginning and deletion in the end of the linked list.


#include<stdio.h>
#include<stdlib.h>
#include<malloc/_malloc.h>




struct Node
{
int data;
struct Node* next;
}*start=NULL,*q,*t;
void insert_beg();
void delete_end();
void display();


int main()
{
 int ch;
 printf("which thing would you like to do\n");
 printf("1.insert in beginning\n");
 printf("2.delete from end\n");
 printf("3.exit\n");
 do
  {
    printf("enter choice\n");
    scanf("%d",&ch);
    switch(ch)
    {
      case 1: insert_beg();break;
      case 2: delete_end();break;
      case 3:printf("you chose to exit\n");break;
      default: printf("an invalid choice\n");
    }
  }while(ch!=3);


return 0;
}




void display()
{
    if(start==NULL)
    {
        printf("List is empty!!");
    }
    else
    {
        q=start;
        printf("The linked list is:\n");
        while(q!=NULL)
        {
            printf("%d->",q->data);
            q=q->next;
        }
    }
    printf("\n");
}


void insert_beg()
{
    int num;
    t=(struct Node*)malloc(sizeof(struct Node));
    printf("Enter data:");
    scanf("%d",&num);
    t->data=num;


    if(start==NULL)        //If list is empty
    {
        t->next=NULL;
        start=t;
    }
    else
    {
        t->next=start;
        start=t;
    }
    display();
}


void delete_end()
{
    if(start==NULL)
    {
        printf("The list is empty!!");
    }
    else
    {
        q=start;
        while(q->next->next!=NULL)
        q=q->next;


        t=q->next;
        q->next=NULL;
        printf("Deleted element is %d",t->data);
        free(t);
    }
    printf("\n");
    display();
}